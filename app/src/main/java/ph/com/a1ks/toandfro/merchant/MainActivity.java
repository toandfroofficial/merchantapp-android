package ph.com.a1ks.toandfro.merchant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.calligraphy3.TypefaceUtils;
import ph.com.a1ks.toandfro.merchant.adapter.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        AHBottomNavigation.OnTabSelectedListener {

    // Setters
    private boolean isCardViewClicked = false;

    // Viewpager
    private MainFragment currentFragment;
    private ViewPagerAdapter viewPagerAdapter;

    // UI Elements
    private ActionBarDrawerToggle toggleBtn;

    // Bindings
    @BindView(R.id.main_drawer)
    DrawerLayout mainDrawer;
    @BindView(R.id.main_toolbar)
    Toolbar mainToolbar;
    @BindView(R.id.main_navigation_view)
    NavigationView mainNavigationView;
    @BindView(R.id.bottom_navigation)
    AHBottomNavigation bottomNavigation;
    @BindView(R.id.view_pager)
    AHBottomNavigationViewPager viewPager;
    @BindView(R.id.main_collapsing_toolbar)
    CollapsingToolbarLayout mainCollapsingToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return toggleBtn.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        if (currentFragment == null) {
            currentFragment = viewPagerAdapter.getCurrentFragment();
        }

        viewPager.setCurrentItem(position, true);
        if (currentFragment == null) {
            return true;
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_store: {
                break;
            }
            case R.id.nav_notification: {
                break;
            }
            case R.id.nav_settings: {
                break;
            }
            case R.id.nav_help: {
                break;
            }
            case R.id.nav_legal: {
                break;
            }
            case R.id.nav_logout: {
                break;
            }
        }
        mainDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    public void expandSingleDetail(CardView singleDeliveryDetails, LinearLayout detailsView) {
        if (isCardViewClicked) {
            TransitionManager.beginDelayedTransition(singleDeliveryDetails);
            detailsView.setVisibility(View.GONE);
            isCardViewClicked = false;
        } else {
            TransitionManager.beginDelayedTransition(singleDeliveryDetails);
            detailsView.setVisibility(View.VISIBLE);
            isCardViewClicked = true;
        }
    }

    private void initUI() {
        //init Navigation Drawer
        mainNavigationView.setItemIconTintList(null);
        mainNavigationView.setNavigationItemSelectedListener(this);
        disableNavigationViewScrollbars(mainNavigationView);

        //init Toolbar
        setSupportActionBar(mainToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.store));
        }
        toggleBtn = new ActionBarDrawerToggle(this, mainDrawer, R.string.open, R.string.close);
        mainDrawer.addDrawerListener(toggleBtn);
        toggleBtn.syncState();

        //init Collapsing
        mainCollapsingToolbar.setCollapsedTitleTypeface(TypefaceUtils.load(getAssets(), "montserrat.ttf"));
        mainCollapsingToolbar.setExpandedTitleTypeface(TypefaceUtils.load(getAssets(), "montserrat.ttf"));

        //init Bottom Navigation and ViewPager
        AHBottomNavigationAdapter navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.bottom_menu_delivery);
        navigationAdapter.setupWithBottomNavigation(bottomNavigation);
        bottomNavigation.setAccentColor(getResources().getColor(R.color.colorAccent));
        bottomNavigation.setInactiveColor(getResources().getColor(R.color.colorDarkGray));
        bottomNavigation.setTitleTypeface(TypefaceUtils.load(getAssets(), "montserrat.ttf"));
        bottomNavigation.setForceTint(true);
        bottomNavigation.setOnTabSelectedListener(this);
        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(3);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        currentFragment = viewPagerAdapter.getCurrentFragment();
    }

}
