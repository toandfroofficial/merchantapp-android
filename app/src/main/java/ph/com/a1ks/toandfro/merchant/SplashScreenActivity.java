package ph.com.a1ks.toandfro.merchant;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class SplashScreenActivity extends AppCompatActivity {

    // Setters
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static int PLAY_SERVICES_RESPONSE = 0;

    // UI Elements
    public MaterialDialog showGooglePlayError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPlayServices();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }

    private void startUps() {
        if (PLAY_SERVICES_RESPONSE == 1) {
            Intent secondTime = new Intent(SplashScreenActivity.this, MainActivity.class);
            startActivity(secondTime);
            finish();
        } else {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                    .title(R.string.google_play_error)
                    .content(R.string.google_play_error_content)
                    .positiveText(android.R.string.ok)
                    .onPositive((dialog, which) -> finishAndRemoveTask());

            showGooglePlayError = builder.show();
        }
    }

    private void checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                Dialog dialog = apiAvailability.getErrorDialog(this, resultCode,
                        PLAY_SERVICES_RESOLUTION_REQUEST);
                if (dialog != null) {
                    dialog.show();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setOnCancelListener(dialog1 -> {
                        if (ConnectionResult.SERVICE_INVALID == resultCode) {
                            finishAndRemoveTask();
                        }
                    });
                    PLAY_SERVICES_RESPONSE = 0;
                    return;
                }
            }
            PLAY_SERVICES_RESPONSE = 0;
            return;
        }
        PLAY_SERVICES_RESPONSE = 1;
        startUps();
    }
}