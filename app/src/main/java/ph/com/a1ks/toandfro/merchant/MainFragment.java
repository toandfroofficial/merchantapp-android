package ph.com.a1ks.toandfro.merchant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainFragment extends Fragment {

    final MainActivity mainActivity = (MainActivity) getActivity();

    public static MainFragment newInstance(int index) {
        MainFragment fragment = new MainFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            if (getArguments().getInt("index", 0) == 0) {
                View view = inflater.inflate(R.layout.fragment_on_progress, container, false);
                initOnProgress(view);
                return view;
            } else if (getArguments().getInt("index", 0) == 1) {
                View view = inflater.inflate(R.layout.fragment_add_delivery, container, false);
                initAddDelivery(view);
                return view;
            } else {
                View view = inflater.inflate(R.layout.fragment_history, container, false);
                initHistory(view);
                return view;
            }
        } else {
            return null;
        }
    }

    private void initOnProgress(View view) {
        final CardView singleDeliveryDetails = view.findViewById(R.id.single_delivery_details);
        final LinearLayout detailsView = view.findViewById(R.id.details_view);
        final ImageView checkDeliveryDetails = view.findViewById(R.id.check_delivery_details);

        singleDeliveryDetails.setOnClickListener(v -> {
            if (mainActivity != null) {
                mainActivity.expandSingleDetail(singleDeliveryDetails, detailsView);
            }
        });
    }

    private void initAddDelivery(View view) {
        final EditText editCity = view.findViewById(R.id.edit_city);
    }

    private void initHistory(View view) {

    }

}
